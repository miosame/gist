#!/bin/sh

# parallelized h264-checker.sh

checkmedia() {
    mediainfo "$1" | grep -i "av1\|vp9" > /dev/null
    if [[ $? != 1 ]]; then
        echo "av1/vp9 detected"
    else
        echo "not av1/vp9"
    fi
}

export -f checkmedia

find /var/music/downloads -name "*.mp4" | parallel -I% -j6 --max-args 1 checkmedia %