# without extracting images
find . -name "*.docx" -type f -execdir pandoc -f docx -t markdown {} -o {}_markdown.md \;

# with extracting images to folder named "images" instead of default "media"
find . -name "*.docx" -type f -execdir pandoc -f docx -t markdown {} -o {}_markdown.md --extract-media=./ \; -execdir mv ./media ./images \;