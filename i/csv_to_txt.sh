# saves the first column and removes quotes
# adjust awk statement for more columns

# expects all .csv in "csv" folder
# expects a "txt" folder to exist

for f in ./csv/*.csv; do
    FILENAME="${f##*/}";
    FILENAME_WITHOUT_EXT="${FILENAME%.*}";
    echo "doing $FILENAME_WITHOUT_EXT"
    awk -F "," '{print $1}' "$f" | tr -d \" > "./txt/$FILENAME_WITHOUT_EXT.txt"
done
