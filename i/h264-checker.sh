#!/usr/bin/env bash
for file in *.mp4
do
    mediainfo "$file" | grep "264" > /dev/null
    if [[ $? != 0 ]]; then
        echo "$file"
    fi
done