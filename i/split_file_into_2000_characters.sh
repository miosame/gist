# 2k = 2kb of data, which roughly translates to around 2000 characters
# handy for when e.g. a service limits how much characters you can enter per message
# -d makes the suffixes be numbers instead of letters, will produce cut-00.txt, cut-01.txt, ..
split -b 2k -d input.txt cut-
