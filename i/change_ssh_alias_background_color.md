## change background color based on ssh alias

That goes into ~/.config/fish/config.fish:

```fish
echo -e "\033]11;#232627\a"

function ssh
    set SSH_PATH (which ssh)
    $SSH_PATH $argv; . ~/.config/fish/config.fish
end
```

Which resets after a ssh session the background color to `#232627` (check what your terminal background is set to)
Then put `PermitLocalCommand yes` at the end of: `/etc/ssh/ssh_config`

And now for the actual sessions in: `~/.ssh/config`, e.g.:

```
Host example
  HostName 127.0.0.1
  User root
  Port 22
  localcommand echo -e "\033]11;#db7093\a"
```

That'll set this session terminal background to `#db7093` when you connect.