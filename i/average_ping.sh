PINGS=()

while :
do
    # do 3 pings, with 1 second inbetween
    # to host: 8.8.8.8
    for i in {1..3}; do
        PING=$(ping -c 1 8.8.8.8 |  awk 'FNR == 2 { print $(NF-1) }' | sed 's/time=//')
        PINGS+=($PING)
        sleep 1
    done

    # output all array items for debugging
    #echo "${PINGS[*]}"

    # add all into a sum
    total=0
    for i in ${PINGS[@]}; do
        total=$(echo "$total+$i" | bc)
    done

    # calculate average and output
    AVG=$(echo "$total/${#PINGS[@]}" | bc)
    echo "Average: $AVG"

    # reset back to empty array
    PINGS=()
done
