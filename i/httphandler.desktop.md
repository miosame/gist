Put into `/usr/share/applications`:

```
[Desktop Entry]
Version=1.0
Name=HTTP Handler
Keywords=Internet;WWW;Browser;Web;Explorer
Exec=/path/to/httphandler.sh %u
Terminal=false
X-MultipleArgs=false
Type=Application
Categories=GNOME;GTK;Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;x-scheme-handler/chrome;video/webm;application/x-xpinstall;
StartupNotify=true
```

Then set as default: `xdg-settings set default-web-browser httphandler.desktop`
Make sure you have the httphandler.sh placed at `/path/to/httphandler.sh`