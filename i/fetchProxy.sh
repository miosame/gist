#!/usr/bin/env bash

refetch=false

# fuck absolute paths not resolving to actual file directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

while getopts "f" opt; do
  case ${opt} in
    f )
        refetch=true
      ;;
    \? )
        echo "Usage: cmd [-f] (refetch proxies, otherwise check worked.txt only)"
        exit 1
      ;;
  esac
done

testProxy() {
   result=$(curl --silent --max-time 2 -x http://$1 https://www.youtube.com/watch?v=H4SLguxm1QQ)
   if [ -n "$result" ]
   then
      if echo "$result" | cut -c1-100 | grep -q "sorry"
      then
         echo "failed: $1 (sorry-bot)"

         #consider tainted instead of retrying
         #echo "$1" >> failed.txt
      else
         echo "worked: $1"
         echo "$1" >> worked.txt
      fi
   else
      echo "failed: $1 (empty-body)"
      echo "$1" >> failed.txt
   fi
}

fetch() {
   # remove duplicates
   sort -u $1 -o $1

   # exec in parallel with max 10 jobs at a time
   cat $1 | parallel --will-cite -j30 testProxy {}

   # remove empty last newline
   perl -pi -e 'chomp if eof' worked.txt
}

retry() {
   echo "--------------- waiting 5 seconds"

   # slep 5 seconds before retry
   sleep 5

   echo "--------------- retrying failed"

   # retry all failed proxies, 2nd chance
   fetch failed.txt
}

export -f testProxy

echo "--------------- initial run"


if [ "$refetch" = true ]; then
   # fetch updated proxies
   curl --silent https://raw.githubusercontent.com/ShiftyTR/Proxy-List/master/https.txt > proxies.txt
   curl --silent https://raw.githubusercontent.com/h4mid007/free-proxy-list/master/proxies.txt >> proxies.txt
   curl --silent https://raw.githubusercontent.com/sunny9577/proxy-scraper/master/proxies.txt >> proxies.txt
   curl --silent https://www.proxyscan.io/download?type=https >> proxies.txt
   curl --silent https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list-raw.txt >> proxies.txt
   curl --silent https://raw.githubusercontent.com/TheSpeedX/PROXY-List/master/http.txt >> proxies.txt

   # clean out previous runs
   > ./worked.txt
   > ./failed.txt

   fetch proxies.txt
   retry
else
   # move to new file
   mv worked.txt worked.tmp.txt

   # clean out previous runs
   > ./worked.txt
   > ./failed.txt

   fetch worked.tmp.txt
   retry
fi