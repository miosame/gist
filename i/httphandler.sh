#!/usr/bin/env bash

# for use in Fedora/KDE via Settings > Default Applications > Web Browser > with the following command

# Arguments: a single URI.
# Will try and open the contents of a remote http(s) URI with specialized
# software before falling back to a web-browser.

[[ "$#" -lt 1 ]] && exit 1

open_in_browser() {
    "${BROWSER}" "${1}"
}

header="$(curl --connect-timeout 5 -sI "${1}")"
regex='[Cc]ontent-[Tt]ype: ([[:alnum:]]+/[[:alnum:]]+)'
[[ "${header}" =~ ${regex} ]] && filetype="${BASH_REMATCH[1]}"

case "${filetype}" in
    image*)
        mpv "${1}"
        ;;
    audio*|video*)
        dragon "${1}"
        ;;
    *)
        open_in_browser "${1}"
        ;;        
esac