alias caddyreload="docker-compose exec caddy-proxy caddy reload --config /etc/caddy/Caddyfile"
alias composer='docker run --rm -v $(pwd):/app composer/composer'
alias dockerips="docker ps -q | xargs -n 1 docker inspect --format '{{ .Name }} {{range .NetworkSettings.Networks}} {{.IPAddress}}{{end}}' | sed 's#^/##';"