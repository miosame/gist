# diffs from the previous state to current HEAD and creates a zip of changed files, ignoring deleted files
git archive -o update.zip HEAD $(git diff --diff-filter=d --name-only <previous-SHA> HEAD)