#!/bin/bash

real_run=false;
RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[1;33m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color

function name_clean() {
    local _out=$(echo "$1" | sed -e 's/\[[^][]*\]//g');

    filename=$(basename -- "$_out");
    extension="${filename##*.}";
    filename="${filename%.*}";
    _out=$filename;

    _out=$(echo "$_out" | sed -e 's/([^()]*)//g');
    _out=$(echo "$_out" | sed 's/_/ /g');
    _out=$(echo "$_out" | xargs);

    echo "$_out.$extension"
}

while getopts "cf:" opt; do
  case ${opt} in
    c )
        real_run=true
      ;;
    f )
        folder=$OPTARG
      ;;
    \? )
        echo "Usage: cmd [-c] (confirm real run | default: dry run) [-f] (specify folder | default: current dir)"
        exit 1
      ;;
  esac
done

# folder set, navigate to it
if [ -n "$folder" ]; then
    cd "$folder"
fi

if [ "$real_run" = false ]; then
    printf "${PURPLE}Note:${NC} Dry run, pass -c to actually rename files\n\n";
fi

shopt -s nullglob # https://superuser.com/a/912099/1135848
for i in *.mkv *.mp4 *.avi; do
    cleaned_name=$(name_clean "$i");
    if [ "$i" == "$cleaned_name" ]; then
        printf "${BLUE}Skipping:${NC} ${YELLOW}$i${NC} already clean filename\n";
    else
        if $real_run; then
            mv "$i" "$cleaned_name";
        else
            printf "${RED}Renamed:${NC} ${YELLOW}$i${NC} to ${YELLOW}$cleaned_name${NC}\n"
        fi
    fi
done;